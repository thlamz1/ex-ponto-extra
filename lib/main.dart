import 'package:flutter/material.dart';
import 'package:ponto_extra/create_card.dart';
import 'package:ponto_extra/user.dart';

void main() {
  runApp(UserApp());
}

class UserApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cadastro Usuário',
      home: MyHomePage(title: 'Cadastro Usuário'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<User> userList = [User('Usuário Inicial', 2015)];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: userList.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              key: Key(userList[index].nome),
              child: ListTile(
                title: Text(userList[index].nome),
                subtitle: Text(userList[index].ano.toString()),
              ),
              onDismissed: (direction) => this.setState(
                () {
                  userList.removeAt(index);
                },
              ),
              background: Container(
                color: Colors.red,
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        child: Icon(Icons.add),
        onPressed: () async {
          User created = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateCard(),
              ));
          if (created != null) {
            this.setState(() {
              userList.add(created);
            });
          }
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
