import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ponto_extra/user.dart';

class CreateCard extends StatelessWidget {
  final usernameController = TextEditingController();
  final userageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: Column(children: [
            Text("Nome do Usuário"),
            TextField(
              controller: usernameController,
            )
          ])),
          Expanded(
              child: Column(children: [
            Text("Ano de nascimento do Usuário"),
            TextField(
              controller: userageController,
              keyboardType: TextInputType.number,
            )
          ]))
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () => Navigator.pop(
                  context,
                  User(usernameController.text,
                      int.parse(userageController.text))),
              child: Text('Confirmar')),
          ElevatedButton(
              onPressed: () => Navigator.pop(context, null),
              child: Text('Cancelar'),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                      (states) => Colors.red)))
        ],
      )
    ]));
  }
}
